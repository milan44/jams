### Version 1.1.0
- ViewHelper can now be registered and used in the Template
- Added Debugger
- Fixed Bug where Liquid renderviewhelper would not work

### Version 1.0.0
- First Version