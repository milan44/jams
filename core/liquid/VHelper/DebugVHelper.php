<?php

namespace liquid\VHelper;

class DebugVHelper
{
    public function render($html)
    {
        \JAMS\Utility\Debugger::var_dump($html);
        return "";
    }
}