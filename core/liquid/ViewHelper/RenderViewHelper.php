<?php

use JAMS\Liquid\Renderer;

class RenderViewHelper extends \JAMS\Liquid\ViewHelper
{
    public function render()
    {
        $options = explode(":", $this->arguments["file"]);
        
        $extension = $options[0];
        
        $template_name = $options[1];
        
        $path = $GLOBALS["VARS"]["EXT"].$extension."/Resources/Private/Templates/".ucfirst($template_name).".html";
        
        if (!file_exists($path))
        {
            throw new \JAMS\Liquid\Exceptions\TemplateNotFoundException("Template \"".$path."\" was not found!");
        }
        
        
        $vars = $this->arguments;
        unset($vars["file"]);
        
        
        $liquid = $GLOBALS["VARS"]["CORE"]."liquid/ViewHelper";
        
        $file = file_get_contents($path);
        
        return Renderer::renderTemplate($file, $vars);
    }
}