<?php

class BeforeRender
{
    public function parse($md)
    {
        return $md;
    }
    public function head($head, $xml)
    {
        if ($xml != null)
            $head = '<title>'.$GLOBALS["CONF"]["SITENAME"]." | ".(string)$xml->title.'</title>'.$head;
        return $head;
    }
}
