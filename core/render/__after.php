<?php

class After
{
    public function parse($md)
    {
        $includer = new Including();
        
        if ($GLOBALS["RENDERING"]["INCLUDE"]["js_foot"])
        {
            $md .= $includer->js_foot();
        }

        return $md;
    }
    public function head($head)
    {
        return $head;
    }
}
