<?php

class Before
{
    public function parse($md)
    {
        \JAMS\CORE\Content::includeElements($md);
        return $md;
    }
    public function head($head)
    {
        $includer = new Including();
        
        if ($GLOBALS["RENDERING"]["INCLUDE"]["js_head"])
        {
            $head .= $includer->js_head();
        }
        if ($GLOBALS["RENDERING"]["INCLUDE"]["css"])
        {
            $head .= $includer->css();
        }
        return $head;
    }
}
