<?php
require($GLOBALS["VARS"]["LIB"]."Parsedown.php");

$file = $GLOBALS["VARS"]["FILE"].$GLOBALS["FE"]["URL"];
$parser = new Parsedown();
$content = "";

include($GLOBALS["RENDERING"]["CORE"]["includer"]);

include($GLOBALS["CMD"]["before_render"]);
include($GLOBALS["CMD"]["after_render"]);

include($GLOBALS["VARS"]["CORE"]."render/__before.php");
include($GLOBALS["VARS"]["CORE"]."render/__after.php");

$before = new BeforeRender();
$after = new AfterRender();

$bef = new Before();
$aft = new After();

$head = "";
$xml = null;

if (file_exists($file."/content.md") && file_exists($file."/data.xml"))
{
    $content = file_get_contents($file."/content.md");
    $xml = new SimpleXMLElement(file_get_contents($file."/data.xml"));
}
else
{
    $content = file_get_contents($GLOBALS["ERROR"]["404"]);
}


// CORE PRE PARSE
$content = $bef->parse($content);
$head = $bef->head($head);

$content = \JAMS\CORE\Globals::getMarkdownBefore()."\n".$content;

// PRE PARSE
$content = $before->parse($content);
$head = $before->head($head, $xml);

$content .= "\n".\JAMS\CORE\Globals::getMarkdownAfter();


// PARSE
$content = $parser->text($content);


$content = \JAMS\CORE\Globals::getHTMLBefore()."\n".$content;


// AFTER PARSE
$content = $after->parse($content);
$head = $after->head($head);


$content .= "\n".\JAMS\CORE\Globals::getHTMLAfter();

// CORE AFTER PARSE
$content = $aft->parse($content);
$head = $aft->head($head);


echo '<html>
<head>'.$head.'</head>
<body>';
echo $content;
echo '</body>';
