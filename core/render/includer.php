<?php

class Including
{
    public function css()
    {
        $all = "";
        $css = $GLOBALS["FE"]["css"];
        if (sizeof($css) > 0)
        {
            foreach($css as $c)
            {
                $all .= '<link rel="stylesheet" href="'.$c.'" />';
            }
        }
        return $all;
    }
    public function js_head()
    {
        $all = "";
        $js_head = $GLOBALS["FE"]["js_head"];
        if (sizeof($js_head) > 0)
        {
            foreach($js_head as $js)
            {
                $all .= '<script src="'.$js.'"></script>';
            }
        }
        return $all;
    }
    public function js_foot()
    {
        $all = "";
        $js_foot = $GLOBALS["FE"]["js_foot"];
        
        if (sizeof($js_foot) > 0)
        {
            foreach($js_foot as $js)
            {
                $all .= '<script src="'.$js.'"></script>';
            }
        }
        return $all;
    }
}