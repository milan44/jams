<?php

$file = $GLOBALS["VARS"]["FILE"].$GLOBALS["FE"]["URL"];
if (file_exists($file."/content.md") && file_exists($file."/data.xml"))
    $GLOBALS["HTTP"]["404"] = true;


$external = array_diff(scandir($GLOBALS["VARS"]["EXT"]), array(".", ".."));
$core = array_diff(scandir($GLOBALS["VARS"]["CORE"]), array(".", "..", "xbase", "render", "conf"));

$x = new SimpleXMLElement(file_get_contents($GLOBALS["VARS"]["CORE"]."conf/CONFIGURATION.xml"));
$disabled = (array)$x->disabled;

$cext = [];

if (sizeof($core) > 0)
{
    foreach($core as $coreext)
    {
        $path = $GLOBALS["VARS"]["CORE"].$coreext."/";
        $xml = getXML($path);

        if ($xml != null && file_exists($path."conf.php"))
        {
            $key = (string) $xml->key;
            $coreext = [
                "path" => $path,
                "depends" => (string) $xml->depends,
            ];
            $cext[$key] = $coreext;
        }
        else
        {
            if ($xml == null)
                throw new Exception("Error loading extension: ".$path." (xml == null)");
            else
                throw new Exception("Error loading extension: ".$path." (conf.php not found)");
        }
    }

    $dep = [];

    foreach($cext as $key => $ext)
    {
        $EXTKEY = $key;
        $EXTDIR = $ext["path"];
        if ($ext["depends"] == "none")
            include($ext["path"]."conf.php");
        else
            $dep[$key] = $ext;
    }
    foreach($dep as $key => $ext)
    {
        $EXTKEY = $key;
        $EXTDIR = $ext["path"];
        include($ext["path"]."conf.php");
    }

}


function clear($empty, $clear)
{
    foreach($empty as $key => $val)
    {
        foreach($clear as $k => $v)
        {
            if ($v == $key)
                unset($empty[$key]);
        }
    }
    return $empty;
}


$cext = [];

if (sizeof($external) > 0)
{
    foreach($external as $externalext)
    {
        $path = $GLOBALS["VARS"]["EXT"].$externalext."/";
        $xml = getXML($path);
        
        if ($xml != null && file_exists($path."conf.php"))
        {
            $key = (string) $xml->key;
            $externalext = [
                "path" => $path,
                "depends" => (string) $xml->depends,
            ];
            $cext[$key] = $externalext;
        }
        else
        {
            if ($xml == null)
                throw new Exception("Error loading extension: ".$path." (xml == null)");
                else
                    throw new Exception("Error loading extension: ".$path." (conf.php not found)");
        }
    }
    
    $cext = clear($cext, $disabled);
    
    $dep = [];
    
    foreach($cext as $key => $ext)
    {
        $EXTKEY = $key;
        $EXTDIR = $ext["path"];
        if ($ext["depends"] == "none")
            include($ext["path"]."conf.php");
            else
                $dep[$key] = $ext;
    }
    foreach($dep as $key => $ext)
    {
        $EXTKEY = $key;
        $EXTDIR = $ext["path"];
        include($ext["path"]."conf.php");
    }
    
}




function getXML($path)
{
    if (!file_exists($path."ext.xml"))
        throw new Exception("Error loading extension: ".$path." (xml not found!)");

    try
    {
        $xml = new SimpleXMLElement(file_get_contents($path."ext.xml"));
        if (empty($xml->depends) || empty($xml->key))
            throw new Exception("Error loading extension: ".$path." (xml broke!)");

        return $xml;
    }
    catch (Exception $e)
    {
        throw new Exception("Error loading extension: ".$path." (xml error [ ".$e->getMessage()." ])");
    }
    return null;
}
