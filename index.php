<?php
include("./lib/JAMS/Error.php");

set_exception_handler("Except");
set_error_handler("Error");

if (!file_exists("./temp"))
    mkdir("./temp");
if (!file_exists("./core"))
    mkdir("./core");
if (!file_exists("./ext"))
    mkdir("./ext");
if (!file_exists("./lib"))
    mkdir("./lib");

include("./GLOBALS.php");


include("./lib/JAMS/autoload.php");


include("./core/xbase/conf.php");

include("./core/render/conf.php");

?>
