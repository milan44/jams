<?php
namespace JAMS\Liquid;

use JAMS\CORE\Repository\PageRepository;

class VariableParser {

    protected $_openingTag = '{{';
    protected $_closingTag = '}}';
    protected $variables;
    protected $_html = "";
    
    /**
     * @var PageRepository $pageRepository
     */
    protected $pageRepository;

    public function __construct($html, $variables) {
        $this->variables = $variables;
        $this->_html = $html;
        
        $this->pageRepository = new PageRepository();
    }

    public function parse() {
        $html = $this->_html;
        
        $vars = $this->pageRepository->getVariableArray();
        
        $this->variables = array_merge($vars, $this->variables);
        
        foreach ($this->variables as $key => $value) {
            $html = str_replace($this->_openingTag . $key . $this->_closingTag, $value, $html);
        }
        return $html;
    }
}