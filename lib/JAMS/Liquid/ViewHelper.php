<?php

namespace JAMS\Liquid;

class ViewHelper
{
    /**
     * @var array
     */
    protected $arguments;
    
    public function __construct($args)
    {
        $this->arguments = array();
        
        foreach($args as $key => $value)
        {
            $this->arguments[$key] = $value;
        }
    }
}