<?php

namespace JAMS\Liquid;

class Renderer
{
    public static function renderTemplate($data, $variables)
    {
        $res = "";
        
        $parser = new \JAMS\Liquid\VariableParser($data, $variables);
        
        $res = $parser->parse();
        
        $vhelp = new \JAMS\Liquid\Templates\VHelper();
        
        $res = $vhelp->parse($res);
        
        return $res;
    }
}