<?php

namespace JAMS\Liquid\Templates;

class VHelper
{
    public function parse($html)
    {
        $helpers = array();
        
        preg_match_all("/<(.*?)>/", $html, $all);
        
        foreach($all[0] as $tag)
        {
            $tag = $tag;
            if (strpos($tag, '/') == false && !in_array($tag, $helpers) && array_key_exists($tag, $GLOBALS["LIQUID"]["VHELPER"])) {
                $helpers[] = $tag;
            }
        }
        
        foreach($helpers as $helper)
        {
            $help = $GLOBALS["LIQUID"]["VHELPER"][$helper];
            include_once($help["PATH"]);
            $full = $help["NS"]."\\".$help["CN"];
            
            $vhelper = new $full();
            
            $closinghelper = str_replace("<", "</", $helper);
            
            $regex = "/".preg_quote($helper)."(.*?)".str_replace("/", "\/", preg_quote($closinghelper))."/";
            
            preg_match_all($regex, $html, $helped);
            
            $helped = $helped[0];
            
            $inner = new \JAMS\Liquid\Templates\VHelper();
            
            foreach($helped as $hel)
            {
                $ppp = $this->str_replace_first($helper, "", $this->str_replace_last($closinghelper, "", $hel));
                
                $ppp_parsed = $inner->parse($ppp);
                
                $before = $hel;
                
                if ($this->startsWith($ppp_parsed, "FILE:"))
                {
                    $args = explode(";", $ppp_parsed);
                    
                    $args[0] = $this->str_replace_first("FILE:", $GLOBALS["VARS"]["FILE"], $args[0]);
                    
                    if (!file_exists($args[0]))
                    {
                        throw new \JAMS\CORE\Exceptions\FileNotFoundException();
                    }
                    
                    $pageRepository = new \JAMS\CORE\Repository\PageRepository($args[0]);
                    
                    $xml = null;
                    
                    if ($args[1] == "page")
                    {
                        $xml = $pageRepository->getXML();
                        $ppp_parsed = $xml;
                    }
                }
                
                $parsed = $vhelper->render($ppp_parsed);
                
                $html = str_replace($before, $parsed, $html);
            }
        }
        
        return $html;
    }
    
    public static function registerVHelper($tag, $path, $namespace, $classname)
    {
        $GLOBALS["LIQUID"]["VHELPER"][$tag] = [
            "PATH" => $path,
            "NS" => $namespace,
            "CN" => $classname,
        ];
    }
    
    protected function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
    protected function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from, '/').'/';
        
        return preg_replace($from, $to, $content, 1);
    }
    protected function str_replace_last($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);
        
        if($pos !== false)
        {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }
        
        return $subject;
    }
}