<?php
namespace JAMS\Utility;

final class Debugger
{
    public static function var_dump($variable)
    {
        $debugger = new \JAMS\Utility\Debugger();
        
        echo '<link rel="stylesheet" href="/lib/JAMS/Utility/debugger.css" />
<div class="debugger">
<h1 class="debugger_title">JAMS Variable Dump</h1>';
        
        if (is_array($variable))
        {
            echo '<p class="debugger_class">Array</p>';
            echo $debugger->renderArray($variable);
        }
        else if (is_bool($variable))
        {
            echo $debugger->renderPrintable($variable);
        }
        else if (is_callable($variable))
        {
            $reflect = new \ReflectionFunction($variable);
            
            echo '<p class="debugger_class">'.str_replace('{closure}', 'function', $reflect->name).' ( ';
            
            $first = true;
            foreach($reflect->getParameters() as $param)
            {
                if ($first)
                {
                    $first = false;
                    echo '$'.htmlentities($param->name);
                }
                else
                {
                    echo ', $'.htmlentities($param->name);
                }
            }
            
            echo ' )</p><br>';
        }
        else if (is_float($variable))
        {
            echo $debugger->renderPrintable($variable);
        }
        else if (is_int($variable))
        {
            echo $debugger->renderPrintable($variable);
        }
        else if (is_null($variable))
        {
            echo $debugger->renderPrintable($variable);
        }
        else if (is_object($variable))
        {
            echo $debugger->renderObject($variable);
        }
        else if (is_string($variable))
        {
            echo $debugger->renderPrintable($variable);
        }
        else
        {
            echo "<h2 class='debugger_unknown'>UNKNOWN VARIABLE (".gettype($variable).")</h2>";
        }
        
        echo '</div>';
    }
    
    protected function isPrintable($variable)
    {
        return is_bool($variable) || is_float($variable) || is_int($variable) || is_null($variable) || is_string($variable);
    }
    
    protected function getPrintable($variable, $bool = true)
    {
        if (is_bool($variable))
        {
            if ($variable)
            {
                $x = "<span class='debugger_bool debugger_out'>TRUE</span>";
                if ($bool)
                    return $x." (boolean)";
                else
                    return $x;
            }
            else
            {
                $x = "<span class='debugger_bool debugger_out'>FALSE</span>";
                if ($bool)
                {
                    return $x." (boolean)";
                }
                else
                {
                    return $x;
                }
            }
        }
        else if (is_float($variable) || is_int($variable))
        {
            $x = "<span class='debugger_number debugger_out'>$variable</span>";
            if ($bool)
            {
                return $x."(".gettype($variable).")";
            }
            else
            {
                return $x;
            }
        }
        else if (is_null($variable))
        {
            return "<span class='debugger_null debugger_out'>NULL</span>";
        }
        else if (is_string($variable))
        {
            $x = "<span class='debugger_string debugger_out'>".htmlentities($variable)."</span>";
            if ($bool)
            {
                return $x."(string)";
            }
            else
            {
                return $x;
            }
        }
        
        return "";
    }
    
    protected function renderPrintable($value)
    {
        $arrow = '<span class="debugger_property_key"></span>';
        
        $res = '<p class="debugger_class">'.ucfirst(gettype($value)).$arrow.$this->getPrintable($value, false).'</p>';
        
        return $res."<br>";
    }
    
    protected function renderValue($value)
    {
        if ($this->isPrintable($value))
        {
            return $this->getPrintable($value);
        }
        
        if (is_object($value))
        {
            return '<span class="debugger_type">'.get_class($value).'</span>';
        }
        
        $res = '<span class="debugger_type">'.gettype($value).'</span>';
        
        if (is_iterable($value))
        {
            $res .= '('.sizeof($value).')';
        }
        
        return $res;
    }
    
    protected function renderArray($array, $class = null)
    {
        $final = '<div class="debugger_properties">';
        
        foreach($array as $key => $value)
        {
            $final .= '<p class="debugger_property"><span class="debugger_property_key">'.$key.'</span><span class="debugger_property_value">'.$this->renderValue($value).'</span>';
            
            if ($class != null)
            {
                $prop = new \ReflectionProperty($class, $key);
                if ($prop->isPublic())
                {
                    $final .= '<span class="debugger_public debugger_typ">public</span>';
                }
                else if ($prop->isProtected())
                {
                    $final .= '<span class="debugger_protected debugger_typ">protected</span>';
                }
                else if ($prop->isPrivate())
                {
                    $final .= '<span class="debugger_private debugger_typ">private</span>';
                }
                
                
                if ($prop->isStatic())
                {
                    $final .= '<span class="debugger_static debugger_typ">static</span>';
                }
            }
            
            $final .= '</p>';
        }
        
        $final .= '</div>';
        return $final;
    }
    
    protected function renderObject($object)
    {
        $type = ucfirst(gettype($object));
        $properties = get_object_vars($object);
        $class = get_class($object);
        
        $final = '<p class="debugger_class">'.$class.'</p>';
        
        $final .= $this->renderArray($properties, $object);
        
        return $final;
    }
}