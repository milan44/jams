<?php
namespace JAMS\Utility;

class XML
{
    
    /**
     * @param \SimpleXMLElement $xmlObject
     */
    public static function toArray($xmlObject)
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
        
        return $out;
    }
}