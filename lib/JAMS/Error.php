<?php
// E R R O R   H A N D L I N G


function Error($code, $text, $file, $line)
{
    echo '<link rel="stylesheet" href="/lib/JAMS/error.css" />';
    echo '
<div id="error">
    Error in file '.htmlentities($file).' on line '.htmlentities($line).'.
    <br>
    <br>
    #'.htmlentities($code).': '.htmlentities($text).'
    <div class="trace_code">
        <p class="trace_code_wrapper">'.getCode($file, $line).'</p>
    </div>
</div>
';
    die();
}





// E X C E P T I O N   H A N D L I N G

/**
 * @param Exception $exception
 */
function Except($exception)
{
    $trace = $exception->getTrace();
    
    echo '<link rel="stylesheet" href="/lib/JAMS/exception.css" />';
    
    echo '<div id="exception">
<h1 class="exception_title">Uncaught Exception</h1>';
    
    //var_dump($exception);
    
    echo '
<div class="exception_wrapper">
    <p class="exception_wrapper_title">'.$exception->getMessage().'</p>
    <br>
    <span class="dark padd">'.get_class($exception).'</span>
    <span class="normal">thrown in file</span>
    <br>
    <span class="dark padd">'.$exception->getFile().'</span><span class="normal">in line</span><span class="dark"> '.$exception->getLine().'</span><span class="normal">.</span>
    <br>
    <br>
    <div class="trace_code">
        <p class="trace_code_wrapper">'.getCode($exception->getFile(), $exception->getLine()).'</p>
    </div>
    <p class="pre"><br></p>
</div>';
    
    
    foreach($trace as $tr)
    {
        //var_dump($tr);
        
        $line = $tr["line"];
        $class = "";
        if (isset($tr["class"]))
            $class = $tr["class"];
        $type = "";
        if (isset($tr["type"]))
            $type = $tr["type"];
        $function = $tr["function"];
        $file = $tr["file"];
        
        
        echo '
<div class="trace">
    <div class="trace_title">
        <span class="trace_line">'.$line.'</span>
        <span class="trace_class">'.$class.'</span>
        <span class="trace_type">'.$type.'</span>
        <span class="trace_function">'.$function.'</span>
        <p>('.getArgs($tr["args"]).')</p>
    </div>
    <div class="trace_code">
        <h2 class="trace_file">'.$file.'</h2>
        <p class="trace_code_wrapper">'.getCode($file, $line).'</p>
    </div>
</div>
';
    }
    echo '</div>';
    die();
}

function fillLine($line)
{
    $line = "$line";
    
    for ($x = strlen($line); $x < 5; $x++)
    {
        $line = "0".$line;
    }
    
    return $line;
}

function getCode($file, $line)
{
    $full = "";
    
    $contents = file($file);
    
    if ($line - 3 >= 0)
    {
        $full .= '<span class="trace_code_notline pre">        '.fillLine($line-3).': '.htmlentities($contents[$line-3]).'</span>';
    }
    if ($line - 2 >= 0)
    {
        $full .= '<span class="trace_code_notline pre">        '.fillLine($line-2).': '.htmlentities($contents[$line-2]).'</span>';
    }
    
    if ($line - 1 >= 0)
    {
        $full .= '<span class="trace_code_line pre">        '.fillLine($line-1).': '.htmlentities($contents[$line-1]).'</span>';
    }
    
    if ($line < sizeof($contents))
    {
        $full .= '<span class="trace_code_notline pre">        '.fillLine($line).': '.htmlentities($contents[$line]).'</span>';
    }
    if ($line + 1 < sizeof($contents))
    {
        $full .= '<span class="trace_code_notline pre">        '.fillLine($line+1).': '.htmlentities($contents[$line+1]).'</span>';
    }
    
    return $full;
}

function getArgs($args = null)
{
    $res = "";
    
    if ($args == null)
    {
        return "";
    }
    
    $res.= '<span class="trace_argument">'.gettype($args[0]).'</span>';
        
    if (sizeof($args) < 1)
    {
        unset($args[0]);
        foreach($args as $arg)
        {
            $res.= ', <span class="trace_argument">'.gettype($arg).'</span>';
        }
    }
    
    return $res;
}