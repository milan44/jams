<?php

spl_autoload_register(
    function ($class_name) {
        if (strpos($class_name, 'JAMS') !== false)
            include str_replace("JAMS/", "", str_replace("\\", "/", $class_name)) . '.php';
        else
        {
            $class = explode("\\", $class_name);
            $name = $class[0]."/Classes/";
            unset($class[0]);
            $class_name = $name.implode("/", $class).".php";
            include($GLOBALS["VARS"]["EXT"].$class_name);
        }
    }
);