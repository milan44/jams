<?php

namespace JAMS\CORE;



class Globals
{
    public static function addToCss($css)
    {
        $GLOBALS["FE"]["css"][] = URL::getRelativeUrl($css);
    }
    public static function addToJSHead($js)
    {
        $GLOBALS["FE"]["js_head"][] = URL::getRelativeUrl($js);
    }
    public static function addToJSFoot($js)
    {
        $GLOBALS["FE"]["js_foot"][] = URL::getRelativeUrl($js);
    }
    public static function prependHTMLToBody($html)
    {
        $GLOBALS["FE"]["body"]["before"][] = [
            "type" => "html",
            "data" => $html,
        ];
    }
    public static function appendHTMLToBody($html)
    {
        $GLOBALS["FE"]["body"]["after"][] = [
            "type" => "html",
            "data" => $html,
        ];
    }
    
    public static function prependMarkdownToBody($md)
    {
        $GLOBALS["FE"]["body"]["before"][] = [
            "type" => "markdown",
            "data" => $md,
        ];
    }
    public static function appendMarkdownToBody($md)
    {
        $GLOBALS["FE"]["body"]["after"][] = [
            "type" => "markdown",
            "data" => $md,
        ];
    }
    
    public static function getMarkdownBefore()
    {
        $res = "";
        foreach($GLOBALS["FE"]["body"]["before"] as $md)
        {
            if ($md["type"] == "markdown")
                $res .= "\n".$md["data"];
        }
        return $res;
    }
    public static function getMarkdownAfter()
    {
        $res = "";
        foreach($GLOBALS["FE"]["body"]["after"] as $md)
        {
            if ($md["type"] == "markdown")
                $res .= "\n".$md["data"];
        }
        return $res;
    }
    
    public static function getHTMLBefore()
    {
        $res = "";
        foreach($GLOBALS["FE"]["body"]["before"] as $html)
        {
            if ($html["type"] == "html")
                $res .= "\n".$html["data"];
        }
        return $res;
    }
    public static function getHTMLAfter()
    {
        $res = "";
        foreach($GLOBALS["FE"]["body"]["after"] as $html)
        {
            if ($html["type"] == "html")
                $res .= "\n".$html["data"];
        }
        return $res;
    }
    
}
