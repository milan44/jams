<?php

namespace JAMS\CORE;

class CustomTags
{
    public static function render($data)
    {
        $html = array(
            "<h1>",
            "<h2>"
        );
        $custom = array(
            "<h2>",
            "<p>"
        );
        
        return preg_replace($html, $custom, $data);
    }
}