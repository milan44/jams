<?php

namespace JAMS\CORE;

class URL
{
    public static function getRelativeUrl($url)
    {
        $dir = str_replace("\\", "/", $GLOBALS["VARS"]["DIR"]);
        $url = str_replace($dir, "", str_replace("\\", "/", $url));
        
        return "/".ltrim($url, "/");
    }
}