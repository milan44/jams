<?php

namespace JAMS\CORE;

class Content
{
    public static function includeElements(&$md)
    {
        $mdBefore = $md;
        
        
        preg_match_all('/\%\%(.*?)\%\%/', $md, $vars);
        unset($vars[0]);
        $vars = $vars[1];
        
        foreach($vars as $var)
        {
            $varb = $var;
            $var = str_replace("\\", "\\\\", $var);
            $var = json_decode('{'.$var.'}');
            
            foreach($var as $v => $k)
            {
                $k = str_replace("\\", "/", $k);
                $md = str_replace("[[".$v.":", "[[EXT:".$k."/ViewHelper/", $md);
            }
            
            
            $md = str_replace("%%".$varb."%%", "", $md);
        }
        
        // LIQUID
        
        $md = str_replace("[[l:", "[[CORE:liquid/ViewHelper/", $md);
        
        
        
        
        
        preg_match_all('/\[\[(.*?)\]\]/', $md, $matches);
        unset($matches[0]);
        $matches = $matches[1];
        foreach ($matches as $match)
        {
            $matchbef = "[[".$match."]]";
            $all = explode(", ", $match, 2);
            
            $all[0] = str_replace(".", "/", $all[0]);
            
            $all[0] = str_replace("CORE:", $GLOBALS["VARS"]["CORE"], $all[0]);
            $all[0] = str_replace("EXT:", $GLOBALS["VARS"]["EXT"], $all[0]);
            
            $temp = str_replace($GLOBALS["VARS"]["CORE"], "CORE/", str_replace($GLOBALS["VARS"]["EXT"], "EXT/", $all[0]));
            
            $temp = explode("/", $temp);
            
            $x = 0;
            
            foreach($temp as $v)
            {
                if ($x > 1)
                    $temp[$x] = ucfirst($v);
                $x++;
            }
            
            $temp = implode("/", $temp);
            
            $all[0] = str_replace("CORE/", $GLOBALS["VARS"]["CORE"], str_replace("EXT/", $GLOBALS["VARS"]["EXT"], $temp));
            
            $ext = $all[0]."ViewHelper.php";
            
            $name = explode("/", $all[0]);
            
            $name = $name[sizeof($name)-1]."ViewHelper";
            
            if (!file_exists($ext))
            {
                $match = "ViewHelper: \"".$ext."\" not found!";
                $md = str_replace($matchbef, $match, $md);
                continue;
            }
            
            include_once($ext);
            
            if (sizeof($all) > 1)
            {
                $args = json_decode($all[1]);
                $renderer = null;
                if (is_object($args))
                    $renderer = new $name($args);
                else
                    $renderer = new $name(array("dynamic" => $all[1]));
                
                $match = $renderer->render();
            }
            else
            {
                $renderer = new $name();
                $match = $renderer->render();
            }
            $md = str_replace($matchbef, $match, $md);
        }
    }
}