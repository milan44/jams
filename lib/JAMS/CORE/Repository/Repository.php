<?php
namespace JAMS\CORE\Repository;

class Repository
{
    /**
     * The Xml of the Repository
     * 
     * @var \SimpleXMLElement $xml
     */
    protected $xml;
    
    public function loadFile($file)
    {
        if (!file_exists($file))
        {
            throw new \JAMS\CORE\Exceptions\RepositoryNotFoundException("Repository was not found!");
        }
        
        $this->xml = new \SimpleXMLElement(file_get_contents($file));
    }
}