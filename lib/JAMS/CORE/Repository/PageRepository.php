<?php
namespace JAMS\CORE\Repository;

class PageRepository extends Repository
{
    public function __construct($file = null)
    {
        if ($file == null)
        {
            $page = $GLOBALS["VARS"]["FILE"].$GLOBALS["FE"]["URL"]."data.xml";
            $this->loadFile($page);
        }
        else
        {
            $this->loadFile($file);
        }
    }
    
    public function getVariableArray()
    {
        $result = array();
        $xml = \JAMS\Utility\XML::toArray($this->xml);
        
        $result["page"] = "FILE:".$GLOBALS["FE"]["URL"]."data.xml;"."page";
        
        foreach($xml as $key => $value)
        {
            if (is_object($value) || is_array($value))
            {
                $result["page.".$key] = "FILE:".$GLOBALS["FE"]["URL"]."data.xml;"."page.".$key;
                $result = $this->getSubArray($value, $result, "page.".$key.".");
            }
            else
            {
                $result["page.".$key] = $value;
            }
        }
        return $result;
    }
    
    public function getXML()
    {
        return $this->xml;
    }
    
    protected function getSubArray($xml, $result, $k)
    {
        foreach($xml as $key => $value)
        {
            if (is_object($value) || is_array($value))
            {
                $result[$k.$key] = "FILE:".$GLOBALS["FE"]["URL"]."data.xml;".$k.$key;
                $result = $this->getSubArray($value, $result, $k.$key.".");
            }
            else
            {
                $result[$k.$key] = $value;
            }
        }
        return $result;
    }
}