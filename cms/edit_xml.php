<?php
session_start();

include("../GLOBALS.php");

if (!isset($_SESSION["login"]) || !$_SESSION["login"])
    header("Location: login.php");

$file = $_GET["file"];
if (strpos($file, 'data.xml') == false)
    header("Location: index.php");
$xml = new SimpleXMLElement(file_get_contents($file));
$title = (string) $xml->title;
$path = str_replace("../files", "", str_replace("data.xml", "", $file));

function is_dir_empty($dir) {
    if (!is_readable($dir)) return NULL;
    return (count(scandir($dir)) == 2);
}
function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    
    return $length === 0 ||
    (substr($haystack, -$length) === $needle);
}

if ($_POST != array())
{
    if (empty($_POST["path"]) || empty(str_replace([" ", "..", "."], ["", "", ""], $_POST["path"])))
        header("Location: edit_xml.php?file=".urlencode($file));
    $cmd = str_replace("data.xml", "", $file)."content.md";
    $dxml = $file;
    
    unlink($file);
    $content = file_get_contents($cmd);
    unlink($cmd);
    if ($path != "/")
    {
        rmdir(str_replace("data.xml", "", $file));
    }
    if ($_POST["delete"] != "on")
    {
        mkdir("../files/".trim(htmlentities(str_replace([" ", "\\", "..", "//"], ["_", "/", "", "/"], $_POST["path"])), "/")."/", 0777, true);
        $path = "../files/".trim(htmlentities(str_replace([" ", "\\", "..", "//"], ["_", "/", "", "/"], $_POST["path"])), "/")."/";
        
        $file = $path."data.xml";
        
        $xml->title = htmlentities($_POST["title"]);
        file_put_contents($file, $xml->asXML());
        file_put_contents($path."content.md", $content);
        header("Location: edit_xml.php?file=".urlencode($file));
    }
    else
    {
        header("Location: index.php");
    }
}
?>
<html>
	<head>
		<title>JAMS | Backend | Edit</title>
		<style>
		  body {
		      margin: 0;
		      font-family: monospace;
		      color: white;
		  }
		  form {
		      display: inline-block;
		      background-color: #2196F3;
		      padding: 20px;
		      margin-left: 50vw;
		      margin-top: 50vh;
		      transform: translateY(-100%) translateX(-50%);
		      border-radius: 2px;
		      box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
		  }
		  table {
		      width: 100%;
		  }
		  td {
		      width: 50%;
		  }
		  .button {
		      font-size: 15px;
		      width: 100%;
		      height: 100%;
		      background: transparent;
		      color: white;
		      border: none;
		      border-radius: 2px;
		      cursor: pointer;
		      padding: 10px;
		      font-weight: bold;
		      font-family: sans-serif;
		  }
		  .green {
		      background: #4CAF50;
		  }
		  .yellow {
		      background: #ff9800;
		  }
		  .red {
		      background: #f44336;
		  }
		</style>
	</head>
	<body>
		<script src="jquery.min.js"></script>
		<form action="edit_xml.php?file=<?php echo urlencode($file); ?>" method="post">
			<table>
				<tr>
					<td>
						<label for="path">Path: </label>
					</td>
					<td>
						<input type="text" name="path" autofocus id="path" value="<?php echo $path; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						<label for="title">Page Title: </label>
					</td>
					<td>
						<input type="text" name="title" id="title" value="<?php echo $title; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						<label for="delete">Delete: </label>
					</td>
					<td>
						<input type="checkbox" name="delete" id="delete" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="Save" class="button green"/>
					</td>
					<td>
						<button type="button" class="button red" onclick="location.href='index.php';">Cancel</button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>