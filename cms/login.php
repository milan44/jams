<?php
session_start();
include("CONFIG.php");

if (isset($_POST["name"]) && isset($_POST["password"]))
{
    if ($_POST["name"] == $USER["name"] && sha1($_POST["password"]) == $USER["password"])
    {
        $_SESSION["login"] = true;
        header("Location: index.php");
    }
    else
    {
        header("Location: login.php");
    }
}
?>
<html>
	<head>
		<title>JAMS | Backend | Login</title>
		<style>
		  body {
		      margin: 0;
		      font-family: monospace;
		      color: white;
		  }
		  form {
		      display: inline-block;
		      background-color: #2196F3;
		      padding: 20px;
		      margin-left: 50vw;
		      margin-top: 50vh;
		      transform: translateY(-100%) translateX(-50%);
		      border-radius: 2px;
		      box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
		  }
		  table {
		      width: 100%;
		  }
		  td {
		      width: 50%;
		  }
		  .button {
		      font-size: 15px;
		      width: 100%;
		      height: 100%;
		      background: transparent;
		      color: white;
		      border: none;
		      border-radius: 2px;
		      cursor: pointer;
		      padding: 10px;
		      font-weight: bold;
		      font-family: sans-serif;
		  }
		  .green {
		      background: #4CAF50;
		  }
		  .yellow {
		      background: #ff9800;
		  }
		  .red {
		      background: #f44336;
		  }
		</style>
	</head>
	<body>
		<script src="jquery.min.js"></script>
		<form action="login.php" method="post">
			<table>
				<tr>
					<td>
						<label for="name">Username: </label>
					</td>
					<td>
						<input type="text" name="name" autofocus id="name" />
					</td>
				</tr>
				<tr>
					<td>
						<label for="password">Password: </label>
					</td>
					<td>
						<input type="password" name="password" autofocus id="password" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="Login" class="button green"/>
					</td>
					<td>
						<button type="button" class="button red" onclick="location.href='/';">Exit</button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>