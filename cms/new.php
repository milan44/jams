<?php
session_start();
if (!isset($_SESSION["login"]) || !$_SESSION["login"])
{
    header("Location: login.php");
}

$path = "../files/newpage";

$x = 0;

if (file_exists($path))
{
    while(true)
    {
        $x++;
        if (!file_exists($path.$x))
        {
            $path = $path.$x;
            break;
        }
    }
}

mkdir($path);

$xml = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xml>
<data>
    <title>'.ucfirst(str_replace("../files/", "", $path)).'</title>
</data>
';


file_put_contents($path."/data.xml", $xml);
file_put_contents($path."/content.md", "");

header("Location: index.php");
?>