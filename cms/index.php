<?php
session_start();
if (!isset($_SESSION["login"]) || !$_SESSION["login"])
    header("Location: login.php");

require("phpFileTree/php_file_tree.php");

function is_dir_empty($dir) {
    if (!is_readable($dir)) return NULL;
    return (count(scandir($dir)) == 2);
}

function clearEmpty($p)
{
    foreach(scandir($p) as $dir)
    {
        if ($dir != "." && $dir != ".." && is_dir($p.$dir))
        {
            if (is_dir_empty($p.$dir))
                rmdir($p.$dir);
            else
                clearEmpty($p.$dir."/");
        }
    }
}

function getEmpty($p)
{
    $res = 0;
    foreach(scandir($p) as $dir)
    {
        if ($dir != "." && $dir != ".." && is_dir($p.$dir))
        {
            if (is_dir_empty($p.$dir))
                $res++;
            else
                $res = $res + getEmpty($p.$dir."/");
        }
    }
    return $res;
}

while (getEmpty("../files/") > 0)
{
    clearEmpty("../files/");
}
?>

<html>
	<head>
		<title>JAMS | Backend | View</title>
		<link rel="stylesheet" href="phpFileTree/styles/default/default.css" />
	</head>
	<body>
		<script src="jquery.min.js"></script>
		<script src="index.js"></script>
		<button id="logout" onclick="location.href = 'logout.php';">Logout</button>
		<button id="logout" onclick="location.href = 'clearcache.php';">Clear Caches</button>
		<button id="logout" onclick="location.href = 'new.php';">New Page</button>
		<?php
		echo php_file_tree("../files", "javascript:clicked('[link]');");
		?>
		<script src="phpFileTree/php_file_tree_jquery.js"></script>
	</body>
</html>