<?php
session_start();
if (!isset($_SESSION["login"]) || !$_SESSION["login"])
{
    header("Location: login.php");
}

$files = glob('../mdtemp/*'); // get all file names
foreach($files as $file){ // iterate files
    if(is_file($file))
        unlink($file); // delete file
}

function is_dir_empty($dir) {
    if (!is_readable($dir)) return NULL;
    return (count(scandir($dir)) == 2);
}

function clearEmpty($p)
{
    foreach(scandir($p) as $dir)
    {
        if ($dir != "." && $dir != ".." && is_dir($p.$dir))
        {
            if (is_dir_empty($p.$dir))
                rmdir($p.$dir);
                else
                    clearEmpty($p.$dir."/");
        }
    }
}

function getEmpty($p)
{
    $res = 0;
    foreach(scandir($p) as $dir)
    {
        if ($dir != "." && $dir != ".." && is_dir($p.$dir))
        {
            if (is_dir_empty($p.$dir))
                $res++;
                else
                    $res = $res + getEmpty($p.$dir."/");
        }
    }
    return $res;
}

while (getEmpty("../files/") > 0)
{
    clearEmpty("../files/");
}

header("Location: index.php");
?>