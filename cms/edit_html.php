<?php
session_start();
if (!isset($_SESSION["login"]) || !$_SESSION["login"])
    header("Location: login.php");

$file = $_GET["file"];
if (strpos($file, 'content.md') == false)
    header("Location: index.php");

$text = (string) file_get_contents($file);

if ($_POST != array())
{
    $text = $_POST["text"];
    file_put_contents($file, $text);
    header("Location: edit_html.php?file=".urlencode($file));
}
$url = str_replace("../files", "", str_replace("content.md", "", $file));
?>
<html>
	<head>
		<title>JAMS | Backend | Edit</title>
		<style>
		  body {
		      margin: 0;
		      font-family: monospace;
		      color: white;
		  }
		  textarea,
		  form {
		      box-sizing: border-box;
		      display: block;
		      width: 100%;
		      height: 100%;
		      margin: 0;
		      padding: 20px;
		      resize: none;
		      font-size: 20px;
		  }
		  textarea {
		      height: calc(100% - 50px);
		  }
		  table {
		      box-sizing: border-box;
		      height: 50px;;
		      width: 100%;
		  }
		  td {
		      width: 33.3%;
		      vertical-align: middle;
		  }
		  .button {
		      font-size: 15px;
		      width: 100%;
		      height: 100%;
		      background: transparent;
		      color: white;
		      border: none;
		      border-radius: 2px;
		      cursor: pointer;
		      padding: 10px;
		      font-weight: bold;
		      font-family: sans-serif;
		  }
		  .green {
		      background: #4CAF50;
		  }
		  .yellow {
		      background: #ff9800;
		  }
		  .red {
		      background: #f44336;
		  }
		  a {
		      padding-top: 12px !important;
		      display: block;
		      box-sizing: border-box;
		      text-decoration: none;
		      text-align: center;
		  }
		</style>
	</head>
	<body>
		<script src="jquery.min.js"></script>
		<form id="form" action="edit_html.php?file=<?php echo urlencode($file); ?>" method="post">
			<textarea name="text" form="form" id="text" autofocus><?php echo $text; ?></textarea>
			<table>
				<tr>
					<td>
						<input type="submit" value="Save" class="button green"/>
					</td>
					<td>
						<button type="button" class="button red" onclick="location.href='index.php';">Cancel</button>
					</td>
					<td>
						<a class="button yellow" href="<?php echo $url; ?>" target="mdmcs">View</a>
					</td>
				</tr>
			</table>
		</form>
		<script src="taboverride.js"></script>
		<script src="jquery.taboverride.min.js"></script>
		<script>
			$('#text').tabOverride();
			$.fn.tabOverride.tabSize(4);
		</script>
	</body>
</html>