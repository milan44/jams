<?php

$GLOBALS = [
    "VARS" => [
        "EXT" => realpath("./ext")."/",
        "CORE" => realpath("./core")."/",
        "LIB" => realpath("./lib")."/",
        "FILE" => realpath("./files")."/",
        "TEMP" => realpath("./temp")."/",
        "DIR" => realpath(".")."/",
    ],
    "FE" => [
        "URL" => trim($_SERVER['REQUEST_URI'], "/"),
        "js_head" => [],
        "js_foot" => [],
        "css" => [],
        "body" => [
            "before" => [],
            "after" => [],
        ],
        "CACHE" => true,
    ],
    "CMD" => [
        "before_render" => realpath("./core/render/before.php"),
        "after_render" => realpath("./core/render/after.php"),
    ],
    "ERROR" => [
        "404" => realpath("./error/404.md"),
    ],
    "CONF" => [
        "SITENAME" => "JAMS"
    ],
    "HTTP" => [
        "404" => false,
    ],
    "RENDERING" => [
        "INCLUDE" => [
            "css" => true,
            "js_head" => true,
            "js_foot" => true,
        ],
        "CORE" => [
            "includer" => realpath("./core")."/render/includer.php",
        ],
        "MARKDOWN" => "Markdown.php", // Other Options: GithubMarkdown.php, MarkdownExtra.php
    ],
    "LIQUID" => [
        "VHELPER" => [],
    ],
];