# `$GLOBALS["FE"]["body"]`

This contains Markdown/Html which will be added at the top/bottom of the body.

To add Markdown/Html to the top of the body use these functions:

**Markdown**
```php
\JAMS\CORE\Globals::prependMarkdownToBody($data)
```
**Html**
```php
\JAMS\CORE\Globals::prependHTMLToBody($data)
```
Where `$data` is pure Markdown/Html.

---
To add Markdown/Html to the bottom of the body use these functions:

**Markdown**
```php
\JAMS\CORE\Globals::appendMarkdownToBody($data)
```
**Html**
```php
\JAMS\CORE\Globals::appendHTMLToBody($data)
```
Where `$data` is pure Markdown/Html.