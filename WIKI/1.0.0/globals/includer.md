# `$GLOBALS["RENDERING"]["CORE"]["includer"]`
This contains the Class that parses the files from $GLOBALS["FE"] (js_head, css, js_foot) to HTML that will be included in the head/foot.

It has to be a absolute path.

Your Class has to be named `Including` with **NO** Namespace.

It has to contain 3 functions:

**`public function css()`**  
This function takes **NO** Arguments and needs to return the HTML-head-Part which includes all CSS files (`... <link rel="stylesheet" href="path/to/some.css" /> ...`).

**`public function js_head()`**  
This function takes **NO** Arguments and needs to return the HTML-head-Part which includes all JavaScript (js_head) files (`... <script src="path/to/some.js"></script> ...`).

**`public function js_foot()`**  
This function takes **NO** Arguments and needs to return the HTML-foot-Part which includes all JavaScript (js_foot) files (`... <script src="path/to/some.js"></script> ...`).