# `$GLOBALS["CMD"]["before_render"]`

This contains the Class that can modify the Markdown before it gets parsed to HTML.

It has to be a absolute path.

Your Class has to be named `BeforeRender` with **NO** Namespace.

It has to contain 2 functions:

**`public function parse($md)`**  
This function takes **1** Argument (The whole Markdown as a string) and needs to return the finished Markdown as a string.

**`public function head($head, $xml)`**  
This function takes **2** Arguments (`$head`= The whole HTML Head as a string, `$xml` = The `data.xml` of the Page as a SimpleXMLElement). It has to return the finished HTML Head as a string.