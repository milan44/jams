# `$GLOBALS["CMD"]["after_render"]`

This contains the Class that can modify the HTML after it got parsed to HTML.

It has to be a absolute path.

Your Class has to be named `AfterRender` with **NO** Namespace.

It has to contain 2 functions:

**`public function parse($html)`**  
This function takes **1** Argument (The whole HTML as a string) and needs to return the finished HTML as a string.

**`public function head($head)`**  
This function takes **1** Arguments (The whole HTML Head as a string). It has to return the finished HTML Head as a string.