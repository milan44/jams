The `$GLOBALS` array is the most important Part of JAMS. It contains all Configuration.

## `$GLOBALS["VARS"]`

|Key|Value|
|--|--|
|EXT|The absolute path to the Extension directory (`ext`)|
|CORE|The absolute path to the Core-Extension directory (`core`)|
|LIB|The absolute path to the Library directory (`lib`)|
|FILE|The absolute path to the Files directory (`files`)|
|TEMP|The absolute path to the Temporary directory (`mdtemp`)|
|DIR|The absolute path to the Main directory (Your installation directory)|

## `$GLOBALS["FE"]`

|Key|Value|
|--|--|
|URL|The Request Url|
|js_head|An array which contains a list of all JavaScript Files to be included in the html head (relative to the Document Root) [Read More...](/WIKI/1.0.0/globals/head.md)|
|css|An array which contains a list of all CSS Files to be included in the html head (relative to the Document Root) [Read More...](/WIKI/1.0.0/globals/head.md)|
|js_foot|An array which contains a list of all JavaScript Files to be included in the html foot (relative to the Document Root) [Read More...](/WIKI/1.0.0/globals/head.md)|
|CACHE|Cache variable|
|body|[Read More...](/WIKI/1.0.0/globals/body.md)|

## `$GLOBALS["CMD"]`

|Key|Value|
|--|--|
|before_render|Contains the path to the pre processor [Read More...](/WIKI/1.0.0/globals/pre.md)|
|after_render|Contains the path to the past processor [Read More...](/WIKI/1.0.0/globals/past.md)|

## `$GLOBALS["ERROR"]`

|Key|Value|
|--|--|
|404|Path to 404 Error Page (Markdown)|

## `$GLOBALS["CONF"]`

|Key|Value|
|--|--|
|SITENAME|Main Site Name (Possible usage in title)|

## `$GLOBALS["HTTP"]`

|Key|Value|
|--|--|
|404|No usage till now|

## `$GLOBALS["RENDERING"]`

|Key|Value|
|--|--|
|INCLUDES|Defines if js and css should be included|
|CORE includer|The Script that includes JS and CSS [Read More...](/WIKI/1.0.0/globals/includer.md)|
|MARKDOWN|Defines which markdown Flavor should be used|