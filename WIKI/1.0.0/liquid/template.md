# Template Rendering

To render a Template you write (`content.md`):

```markdown
[[l:render, {"file": "your_ext_folder_name:your_template_name", "myvar": "myvalue"}]]
```

All your Templates have to be inside `your_ext_folder_name/Resources/Private/Templates`. The first Character of the name of your Template has to be uppercase and it has to be a HTML File.

You can pass Variables to the Template (See above `"myvar": "myvalue"`).

In the Example above, `{{myvar}}` will be replaced with `myvalue`.
