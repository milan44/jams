# Backend Login

## Default

|Username|Password|
|--|--|
|admin|admin1234|

You can only configure 1 Backend User.

The Username and password can be changed in `cms/CONFIG.php`. The Password has to be SHA-1 encrypted with the PHP function `sha1()`.