To create an Extension you have to create a Folder inside the `ext` Directory of your JAMS installation. This folder needs to contain `ext.xml` and `conf.php`.

### ext.xml

```
<?xml version="1.0" encoding="UTF-8"?>
<extension>
    <key>your_ext_key</key>
    <depends>one extension this extension depends on</depends>
</extension>
```

Replace `your_ext_key` with your extension name (also used in `depends`). Inside of `depends` you can write **ONE** extension this extension depends on. If your extension depends on **NO** other extension you **NEED** to put in: `none`. (e.g.: `<depends>none</depends>`)

### conf.php

This PHP File is the only one that will be included at load of your extension.

### Disable an Extension

To disable an extension you just have to add `<ext>key</ext>` to `core/conf/CONFIGURATION.xml` where `key` is the **key** of the extension you want to disable. **You can't disable Core extensions!**

**Example:**  
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xml>
<configuration>
    <disabled>
        <ext>my_unwanted_extension</ext>
    </disabled>
</configuration>
```