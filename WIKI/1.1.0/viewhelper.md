To use a ViewHelper you first have to specify a namespace at the top of your `content.md`.

```
%%"m": "my_extension_folder"%%
```

The Above example can now be used like this:

```markdown
[[m:testname, {"myvar": "myval"}]]
```

The path to your View Helper would now look like this: `my_extension_folder/ViewHelper/TestnameViewHelper.php`.

Your ViewHelper Class has to extend `\MDCMS\Liquid\ViewHelper`. It needs the public (not static) function `render()` which takes no arguments.

All passed variables are inside `$this->arguments`.

In this case `$this->arguments["myvar"]` would be `myval`.

The Part after the extension (`{"myvar": "myval"}`) is pure Json.