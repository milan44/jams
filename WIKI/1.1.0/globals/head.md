# `$GLOBALS["FE"]` (js_head, css, js_foot)

To add custom JavaScript/CSS to the HTML head of all pages use the following functions:

**JavaScript**
```php
\JAMS\CORE\Globals::addToJSHead($path)
```
**CSS**
```php
\JAMS\CORE\Globals::addToCss($path)
```
Where `$path` is the absolute Path to your css/js file.

---
To add custom JavaScript to the HTML foot of all pages use the following function:

**JavaScript**
```php
\JAMS\CORE\Globals::addToJSFoot($path)
```
Where `$path` is the absolute Path to your js file.