# Installation

Your server needs to fulfill the [Requirements](https://gitlab.com/milan44/jams#requirements).

The installation is very easy. Just unzip the `release X.X.X.zip` you downloaded in your htdocs folder and your good to go.

[Getting Started](/WIKI/1.0.1/getting-started.md)