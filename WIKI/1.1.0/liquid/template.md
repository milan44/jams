# Template Rendering

To render a Template you write (`content.md`):

```markdown
[[l:render, {"file": "your_ext_folder_name:your_template_name", "myvar": "myvalue"}]]
```

All your Templates have to be inside `your_ext_folder_name/Resources/Private/Templates`. The first Character of the name of your Template has to be uppercase and it has to be a HTML File.

You can pass Variables to the Template (See above `"myvar": "myvalue"`).

In the Example above, `{{myvar}}` will be replaced with `myvalue`.

You can use you own custom `VHelper` in this template. Liquid has some pre installed ones like `<f:debug></f:debug>` which will debug everything between the tags using `\JAMS\Utility\Debugger::var_dump()`.

To register your own `VHelper` you use `\JAMS\Liquid\Templates\VHelper::registerVHelper()`.

The following Example shows how the Liquid Debug `VHelper` is registered:

```
\JAMS\Liquid\Templates\VHelper::registerVHelper("<l:debug>", $GLOBALS["VARS"]["CORE"]."liquid/VHelper/DebugVHelper.php", "liquid\VHelper", "DebugVHelper");
```

The first parameter is the tag (e.g. `"<l:debug>"`),  
the second parameter is the absolute path to the `VHelper` PHP file (e.g. `$GLOBALS["VARS"]["CORE"]."liquid/VHelper/DebugVHelper.php"`),  
the third parameter is the namespace (e.g. `liquid\VHelper`)  
and the fourth and last parameter is the ClassName of your VHelper (e.g. `DebugVHelper`).