[![Codefresh build status]( https://g.codefresh.io/api/badges/pipeline/milan44/milan44%2FJAMS%2FJAMS?branch=master&key=eyJhbGciOiJIUzI1NiJ9.NWI3NTRiYWUxNjk4NzcwMDAxNjY2NWRk.NDHvMg4ftYPZ2F8WGDzomyPkxO5eErmCvlLSgixGoNQ&type=cf-1)]( https://g.codefresh.io/repositories/milan44/JAMS/builds?filter=trigger:build;branch:master;service:5b7566e35904b840a371a7e3~JAMS)

# JAMS \[`ʤæms`\] (Just Another Management System)

[Installation](https://gitlab.com/milan44/jams/blob/master/WIKI/1.1.0/installation.md)  
[Getting Started](https://gitlab.com/milan44/jams/blob/master/WIKI/1.1.0/getting-started.md)  

**JAMS** is an Open Source Content Management System based on Markdown and XML. It does not require a Database in any form.

---

### Requirements

* JAMS requires a web server which can run PHP (e.g. Apache, Nginx or IIS).
* JAMS requires at least PHP 7.x.
* JAMS needs to be installed in the DocumentRoot

---

## Releases

### [Changelog](CHANGELOG.md)

### Latest Release (1.1.0) [zip](https://gitlab.com/milan44/jams/raw/release/release%201.1.0.zip)


## Older Versions

* Version 1.1.0 [zip](https://gitlab.com/milan44/jams/raw/release/release%201.1.0.zip)
* Version 1.0.0 [zip](https://gitlab.com/milan44/jams/raw/release/release%201.0.0.zip)